package abstractFactory

/**
 * Created by ROM_PC on 10.04.2016.
 */
abstract class AbstractProductB {
    abstract fun getName() : String
}