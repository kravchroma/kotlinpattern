package abstractFactory

/**
 * Created by ROM_PC on 10.04.2016.
 */
abstract class AbstractFactory {
    abstract fun createA() : AbstractProductA
    abstract fun createB() : AbstractProductB
}
