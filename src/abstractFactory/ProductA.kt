 package abstractFactory

/**
 * Created by ROM_PC on 10.04.2016.
 */
class ProductA : AbstractProductA() {

    companion object{
        val NAME : String = "productA"
    }

    override fun getName(): String {
        return NAME;
    }
}