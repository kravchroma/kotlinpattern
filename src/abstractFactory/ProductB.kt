package abstractFactory

/**
 * Created by ROM_PC on 10.04.2016.
 */
class ProductB : AbstractProductB() {

    companion object {
        val NAME = "productB"
    }

    override fun getName(): String {
        return NAME;
    }
}