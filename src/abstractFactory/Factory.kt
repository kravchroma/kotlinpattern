package abstractFactory

/**
 * Created by ROM_PC on 10.04.2016.
 */
class Factory : AbstractFactory() {
    override fun createA(): AbstractProductA {
        return ProductA()
    }

    override fun createB(): AbstractProductB {
        return ProductB()
    }
}