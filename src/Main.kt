import abstractFactory.AbstractFactory
import abstractFactory.AbstractProductA
import abstractFactory.AbstractProductB
import abstractFactory.Factory
import builder.BuilderA
import builder.BuilderB
import builder.Director
import observer.MyObservable
import observer.Observer


/**
 * Created by ROM_PC on 10.04.2016.
 */
fun main(args: Array<String>) {

    val factory : AbstractFactory = Factory()
    val pA : AbstractProductA = factory.createA()
    val pB : AbstractProductB = factory.createB()
    println(pA.getName());
    println(pB.getName())

    val factory1  = Factory()
    val pA1 = factory1.createA()
    val pB1 = factory1.createB()
    println(pA1.getName());
    println(pB1.getName())
    println("===========")

    val director = Director()
    val listBuilder = arrayListOf(BuilderA(), BuilderB())
    listBuilder.forEach {
        director.abstractBuilder = it;
        director.constructorProduct()
        val product = director.getProduct();
        println(product?.par1)
        println(product?.par2)
        println(product?.par3)
        println(product?.par4)
        println("===========")
    }

    val myObservable = MyObservable()
    val o: Observer = object : Observer {
        override fun event() {
            println("event")
        }
    }
    myObservable.registerObserver(o)

    myObservable.notifyObservers()
    myObservable.notifyObservers()
    myObservable.removeObserver(o)
    myObservable.notifyObservers()
}


