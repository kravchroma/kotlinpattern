package builder

/**
 * Created by ROM_PC on 10.04.2016.
 */
abstract class AbstractBuilder {

    var product:Product? = null
    protected set
        get() { return field }

    fun creteProduct() {
        product = Product();
    }

    abstract fun buildPar1()
    abstract fun buildPar2()
    abstract fun buildPar3()
    abstract fun buildPar4()
}