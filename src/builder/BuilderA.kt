package builder

/**
 * Created by ROM_PC on 10.04.2016.
 */
class BuilderA : AbstractBuilder() {
    override fun buildPar1() {
        product!!.par1 = "P1A"
    }

    override fun buildPar2() {
        product!!.par2 = "P2A"
    }

    override fun buildPar3() {
        product!!.par3 = "P3A"
    }

    override fun buildPar4() {
        product!!.par4 = "P4A"
    }
}