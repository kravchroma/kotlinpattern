package builder

/**
 * Created by ROM_PC on 10.04.2016.
 */
class BuilderB : AbstractBuilder() {
    override fun buildPar1() {
        product!!.par1 = "P1B"
    }

    override fun buildPar2() {
        product!!.par2 = "P2B"
    }

    override fun buildPar3() {
        product!!.par3 = "P3B"
    }

    override fun buildPar4() {
        product!!.par4 = "P4B"
    }
}