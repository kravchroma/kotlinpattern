package builder

/**
 * Created by ROM_PC on 10.04.2016.
 */
class Director {
    var abstractBuilder: AbstractBuilder? = null

    fun getProduct() : Product? {
        return abstractBuilder!!.product
    }

    fun constructorProduct() {
        abstractBuilder?.creteProduct()
        abstractBuilder?.buildPar1()
        abstractBuilder?.buildPar2()
        abstractBuilder?.buildPar3()
        abstractBuilder?.buildPar4()
    }
}