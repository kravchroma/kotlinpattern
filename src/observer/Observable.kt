package observer

/**
 * Created by ROM_PC on 10.04.2016.
 */
interface Observable {
    fun registerObserver(o: Observer);
    fun removeObserver(o: Observer);
    fun notifyObservers();
}