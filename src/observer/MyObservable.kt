package observer

import java.util.*

/**
 * Created by ROM_PC on 10.04.2016.
 */
class MyObservable : Observable {

    private var observers:MutableList<Observer>

    constructor() {
        observers = mutableListOf()
    }

    override fun registerObserver(o: Observer) {
        observers.add(o)
    }

    override fun removeObserver(o: Observer) {
        observers.remove(o)
    }

    override fun notifyObservers() {
        observers.forEach { it.event() }
    }
}