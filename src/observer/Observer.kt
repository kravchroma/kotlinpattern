package observer

/**
 * Created by ROM_PC on 10.04.2016.
 */
interface Observer {
    fun event()
}